class RailsFourCommandGenerator {
  constructor(unitTestCommand, systemTestCommand, specCommand, printAndSavePath = null) {
    this.unitTestCommand = unitTestCommand
    this.systemTestCommand = systemTestCommand
    this.specCommand = specCommand
    this.printAndSavePath = printAndSavePath
  }

  getTestCommand(fileName, type, lineNumber = null, testName = null) {
    let command

    switch(type) {
      case 'unit-test':
        command = this.getUnitTestCommand(fileName, lineNumber, testName)
        if(this.printAndSavePath) command = `${this.printAndSavePath} ${command}`

        return command
      case 'rspec':
        command = `${this.specCommand} ${fileName}`
        if(lineNumber) command += `:${lineNumber}`
        if(this.printAndSavePath) command = `${this.printAndSavePath} ${command}`

        return command
      case 'all-unit-tests':
        command = this.getUnitTestCommand().trim()
        if(command.slice(-1)[0] == '=') {
          let commandArray = command.split(' ')
          commandArray.pop()
          command = commandArray.join(' ')
        }

        return command
      case 'all-system-tests':
        return `${this.systemTestCommand}`
    }
  }

  getUnitTestCommand(fileName = null, lineNumber = null, testName = null) {
    let command
  
    command = this.unitTestCommand
    if(command.slice(-1)[0] != '=') command += ' '

    if(fileName) command += fileName
    if(testName) command += ` TESTOPTS="-n='/${testName}$/'"`

    return command
  }
}

module.exports = {
  RailsFourCommandGenerator
}
