const vscode = require('vscode')

class VscodeTerminalAdapter {
  constructor() {}

  run(command, withShow = true) {
    let terminal = vscode.window.activeTerminal

    if(!terminal) terminal = vscode.window.createTerminal()

    if(withShow) terminal.show()
    terminal.sendText(command)
  }

  iterm() {
    return false
  }

  vscode() {
    return true
  }
}

module.exports = {
  VscodeTerminalAdapter
}
