{
  "name": "ruby-test-runner",
  "displayName": "Ruby Test Runner",
  "publisher": "MateuszDrewniak",
  "description": "Run unit-test and rspec tests with one click!",
  "version": "0.3.8",
  "engines": {
    "vscode": "^1.55.0"
  },
  "icon": "images/icon.png",
  "repository": {
    "type": "git",
    "url": "https://gitlab.com/mateuszdrewniak/ruby-test-runner"
  },
  "categories": [
    "Other"
  ],
  "keywords": [
    "ruby",
    "test",
    "tests",
    "ruby tests",
    "run",
    "ruby run",
    "runner",
    "ruby runner",
    "ruby test runner"
  ],
  "galleryBanner": {
    "color": "#670500",
    "theme": "dark"
  },
  "activationEvents": [
    "onLanguage:ruby"
  ],
  "main": "./extension.js",
  "contributes": {
    "commands": [
      {
        "command": "ruby-test-runner.run-current-test-file",
        "title": "Ruby: Run current test file"
      },
      {
        "command": "ruby-test-runner.run-current-test-file-icon",
        "title": "Ruby: Run test file",
        "category": "Ruby",
        "icon": {
          "light": "resources/light/run-file.svg",
          "dark": "resources/dark/run-file.svg"
        }
      },
      {
        "command": "ruby-test-runner.run-unit-test",
        "title": "Ruby: Run unit-test file"
      },
      {
        "command": "ruby-test-runner.run-rspec",
        "title": "Ruby: Run rspec file"
      },
      {
        "command": "ruby-test-runner.run-all-unit-tests",
        "title": "Ruby: Run unit tests"
      },
      {
        "command": "ruby-test-runner.run-all-system-tests",
        "title": "Ruby: Run system tests"
      },
      {
        "command": "ruby-test-runner.run-test-code-lens'",
        "title": "Ruby: Run test code lens"
      }
    ],
    "menus": {
      "editor/title/run": [
        {
          "command": "ruby-test-runner.run-current-test-file-icon",
          "title": "Ruby: Run test file",
          "group": "navigation",
          "when": "resourceLangId == ruby && !isInDiffEditor && resourceFilename =~ /.*_(test|spec).rb/"
        }
      ]
    },
    "configuration": {
      "title": "Ruby Test Runner",
      "properties": {
        "rubyTestRunner.openTestsInIterm": {
          "type": "boolean",
          "default": false,
          "description": "Open tests in iTerm instead of the VSCode terminal"
        },
        "rubyTestRunner.liveTestResultsEnabled": {
          "type": "boolean",
          "default": true,
          "description": "Generate live test result feed in the editor"
        },
        "rubyTestRunner.systemTestCommand": {
          "type": "string",
          "default": "bundle exec rails test:system",
          "description": "Command which will be used to run system tests _test.rb files"
        },
        "rubyTestRunner.systemTestCommandWithoutRails": {
          "type": "string",
          "default": "bundle exec rspec",
          "description": "Command which will be used to run system tests _test.rb files in projects without Rails system tests"
        },
        "rubyTestRunner.unitTestCommand": {
          "type": "string",
          "default": "bundle exec rails t",
          "description": "Command which will be used to run regular unit _test.rb files"
        },
        "rubyTestRunner.unitTestCommandRails4": {
          "type": "string",
          "default": "bundle exec rake test TEST=",
          "description": "Command which will be used to run regular unit _test.rb files in Rails < 5 projects"
        },
        "rubyTestRunner.specCommand": {
          "type": "string",
          "default": "bundle exec rspec",
          "description": "Command which will be used to run _spec.rb test files"
        },
        "rubyTestRunner.recognizedTestMethodNames": {
          "type": "string",
          "default": "should, scenario, context, describe, it, test",
          "description": "Method names which the extension will recognize as test definitions, seperate with a comma (they have to take a block and the first parameter has to be a string)"
        },
        "rubyTestRunner.runTestCodeLensEnabled": {
          "type": "boolean",
          "default": true,
          "description": "Choose whether the dynamic 'Run test' code lenses in the source code view shall be generated"
        },
        "rubyTestRunner.runAllUnitTestsButtonEnabled": {
          "type": "boolean",
          "default": true,
          "description": "Choose whether the 'Unit tests' Status Bar Button should be shown"
        },
        "rubyTestRunner.runAllSystemTestsButtonEnabled": {
          "type": "boolean",
          "default": true,
          "description": "Choose whether the 'System tests' Status Bar Button should be shown"
        },
        "rubyTestRunner.runTestButtonEnabled": {
          "type": "boolean",
          "default": false,
          "description": "Choose whether the 'Run test file' Status Bar Button should be shown"
        }
      }
    },
    "keybindings": [
      {
        "key": "ctrl+shift+t",
        "command": "ruby-test-runner.run-current-test-file",
        "when": "editorTextFocus"
      },
      {
        "key": "ctrl+shift+a",
        "command": "ruby-test-runner.run-all-unit-tests",
        "when": "editorTextFocus"
      }
    ]
  },
  "scripts": {
    "lint": "eslint .",
    "pretest": "yarn run lint",
    "test": "node ./test/runTest.js"
  },
  "devDependencies": {
    "@types/vscode": "^1.54.0",
    "@types/glob": "^7.1.3",
    "@types/mocha": "^8.0.4",
    "@types/node": "^12.11.7",
    "eslint": "^7.19.0",
    "glob": "^7.1.6",
    "mocha": "^8.2.1",
    "typescript": "^4.1.3",
    "vscode-test": "^1.5.0"
  }
}
